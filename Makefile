# Custom params for docker-compose up
PARAMS=

# Custom ENV vars
ENV=

# ------------------------ All in one (Loki stack) ------------------------#
.PHONY: wapiCore-run-silent wapiCore-run wapiCore-stop wapiCore-rm

wapiCore-run-silent: PARAMS:=-d ${PARAMS}
wapiCore-run-silent: wapiCore-run
wapiCore-run:
	${ENV} docker-compose up ${PARAMS}

wapiCore-stop:
	${ENV} docker-compose stop

wapiCore-rm:
	@printf "This action will delete containers and all associated data.\nAre you sure ? (y/N)?" && read choice && [ $${choice:-N} = y ] &&\
	${ENV} docker-compose down -v


# ------------------------ Loki ------------------------#
.PHONY: loki-run-silent loki-run loki-stop loki-rm loki-shell

loki-run-silent: PARAMS:=-d ${PARAMS}
loki-run-silent: loki-run
loki-run:
	${ENV} docker-compose up ${PARAMS} loki

loki-stop: 
	${ENV} docker-compose stop loki

loki-shell:
	@docker-compose exec -u=root loki /bin/sh
	@true 


# ------------------------ Grafana ------------------------#
.PHONY: grafana-run-silent grafana-run grafana-stop grafana-rm grafana-shell

grafana-run-silent: PARAMS:=-d ${PARAMS}
grafana-run-silent: grafana-run
grafana-run:
	${ENV} docker-compose up ${PARAMS} grafana

grafana-stop: 
	${ENV} docker-compose stop grafana

grafana-shell:
	@docker-compose exec grafana /bin/bash
	@true 


# ------------------------ Other ------------------------#
.PHONY: clean
clean: wapiCore-rm	# Alias to remove everything